# Docs

Repositório para armazenar um conjunto de documentos desenvolvidos por Jesimar da Silva Arantes.

Entre os principais documentos destacam-se as apresentações e palestras em eventos, meus trabalhos de conclusão de cursos (TCC), minha dissertação de mestrado e tese de doutorado.
